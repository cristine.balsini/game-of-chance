
function random() {
  return Math.floor(Math.random() * 3 + 1);
}

let resulUser = 0;    //pontuação do user
let resulSystem = 0;  //pontuação do system


////////////////////      botao pedra       ////////////////////////
let pedra = document.getElementById('pedra');         
pedra.addEventListener('click', function one() {

  userTemplate('img/pedra1.png', 'pedra');                //imagem da escolha do user
  
  if (random() === 1) {                                   //o numero gerado pela função random é o resultado do system
    systemTemplate('img/papel1.png', 'papel')             //imagem do papel pois defini que número 1 é vitória do system
    textUser('Loser!');                                   //imprime texto resultado na tela do user
    textSystem('Winner!');                                //imprime texto resultado na tela do system
    resulSystem++;
    systemScore(resulSystem);                             //imprime contador na tela
  }
  else if (random() === 2) {
    systemTemplate('img/tesoura1.png', 'tesoura');
    textUser('Winner!');
    resulUser++;
    userScore(resulUser);
    textSystem('Loser!');
  }
  else {
    systemTemplate('img/pedra1.png', 'pedra');
    textUser('A Tie!');
    textSystem('A Tie!');
  }
});


////////////////////      botao papel       ////////////////////////
let papel = document.getElementById('papel');
papel.addEventListener('click', function two() {
  
  userTemplate('img/papel1.png', 'papel')

  if (random() === 1) {
    systemTemplate('img/tesoura1.png', 'tesoura');
    textUser('Loser!'); 
    textSystem('Winner!');  
    resulSystem++;
    systemScore(resulSystem);
  }
  else if (random() === 2) {
    systemTemplate('img/pedra1.png', 'pedra');
    textUser('Winner!');
    resulUser++;
    userScore(resulUser);
    textSystem('Loser!');
  }
  else {
    systemTemplate('img/papel1.png', 'papel')
    textUser('A Tie!');;
    textSystem('A Tie!');
  }
});


////////////////////      botao tesoura       ////////////////////////
let tesoura = document.getElementById('tesoura');
tesoura.addEventListener('click', function three() {

  userTemplate('img/tesoura1.png', 'tesoura');

  if (random() === 1) {
    systemTemplate('img/pedra1.png', 'pedra');
    textUser('Loser!');
    textSystem('Winner!');
    resulSystem++;
    systemScore(resulSystem);
    console.log('system:' + resulSystem);
  }
  else if (random() === 2) {
    systemTemplate('img/papel1.png', 'papel')
    textUser('Winner!');
    resulUser++;
    userScore(resulUser);
    textSystem('Loser!');
  }
  else {
    systemTemplate('img/tesoura1.png', 'tesoura');
    textUser('A Tie!');
    textSystem('A Tie!');
  }
});


///////////////////////////////      função que gera imagem da escolha do usuário e do random do computador         ////////////////////////////////////////

function userTemplate (img,alt){                
square1.innerHTML = '';
let imagem = document.createElement ('img');
imagem.setAttribute('src', img);
imagem.setAttribute('alt', alt);
imagem.className = 'dinamicImage';
square1.appendChild(imagem);
}
function systemTemplate (img,alt){                
  square3.innerHTML = '';
  let imagem = document.createElement ('img');
  imagem.setAttribute('src', img);
  imagem.setAttribute('alt', alt);
  imagem.className = 'dinamicImage';
  square3.appendChild(imagem);
  }


  ///////////////////////////////      função que gera as menesagens dos resultados dos usuários e do random do computador         /////////////////////////////

  function textUser(content){
    messageUser.innerHTML = '';
    let message = document.createElement ('h2');
    message.className = 'dinamicUser';
    message.textContent = content;
    messageUser.appendChild(message);
  }
  function textSystem(content){
    messageSystem.innerHTML = '';
    let message = document.createElement ('h2');
    message.className = 'dinamicSystem';
    message.textContent = content;
    messageSystem.appendChild(message);
  }


  //////////////////////////////////  função que gera a pontuação na tela do usuário e do computador   ///////////////////////////
  
  function userScore(count){
    countUser.innerHTML = '';
    let resultado = document.createElement ('div');
    resultado.textContent = count;
    countUser.appendChild(resultado);
  }
  function systemScore(count){
    countSystem.innerHTML = '';
    let resultado = document.createElement ('div');
    resultado.textContent = count;
    countSystem.appendChild(resultado);
  }